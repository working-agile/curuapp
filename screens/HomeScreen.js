//comentario 1.60
import React, { useState, useEffect, useLayoutEffect, useReducer } from 'react';
import { Text, StyleSheet, TouchableOpacity, SafeAreaView, View, ScrollView } from 'react-native';
import Constants from 'expo-constants';

import BackButton from '../components/BackButton';
import AsyncStorage from '@react-native-async-storage/async-storage';


export default function HomeScreen({ navigation }) {

  const [user, setUser] = useState({});
  const [messages, setMessages] = useState([]);

  function goToClientDetails() {
    navigation.navigate("ClientDetailsScreen")
  }


  function goToComplaintPage() {
    navigation.navigate("ComplaintScreen")
  }


  async function getClientMessages() {

    let numberOfMessages
    try {
      numberOfMessages = await AsyncStorage.getItem('numberOfMessages')
    } catch (e) {
      console.log(e);
    }

    console.log("number of messages: " + JSON.stringify(numberOfMessages))


    let newMessages = []
    if (numberOfMessages > 0) {
      for (let i = 1; i <= numberOfMessages; i++) {

        let message
        try {
          message = JSON.parse(await AsyncStorage.getItem('message-' + i))
        } catch (e) {
          console.log(e);
        }

        console.log("Message-" + i + " " + JSON.stringify(message))
        newMessages.push(message)
      }
    }
    setMessages(newMessages)
    console.log("Storage: " + JSON.stringify(messages))
  }

  useLayoutEffect(() => {

    getClientMessages()
  }, []);



  return (
    <View style={styles.container}>
      <BackButton color='#000000' navigation={navigation} homePage={true} />

      <TouchableOpacity
        style={[styles.btn]}
        onPress={() => goToClientDetails()}>
        <Text style={{ color: '#fff', fontWeight: "bold", fontSize: 18, padding: 8 }}>Detalhes do Cliente</Text>
      </TouchableOpacity>

      <TouchableOpacity
        style={[styles.btn]}
        onPress={() => goToComplaintPage()}>
        <Text style={{ color: '#fff', fontWeight: "bold", fontSize: 18, padding: 8 }}>Realizar Denúncia</Text>
      </TouchableOpacity>

      <View style={{ backgroundColor: '#d8f3dc', width: '100%', bottom: 0, position: 'absolute', minHeight: 300 }}>
        <Text style={{ color: '#000', fontWeight: "bold", fontSize: 25, padding: 8 }}>Histórico de Mensagens:</Text>

        {messages.map((message, i) => {
          return (
            <View key={i} style={styles.item}>
              <Text style={styles.subject}>{message.subject}</Text>
              <Text style={styles.content}>{message.content}</Text>
              <Text style={styles.content}> {message.date} </Text>
            </View>
          );
        })}
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: Constants.statusBarHeight,
    backgroundColor: '#fff',
    justifyContent: 'center',
    alignItems: 'center',
    padding: 8
  },
  txt: {
    fontSize: 30,
    fontWeight: 'bold',
    textAlign: 'center',
    marginBottom: 20,
    color: '#000000'
  },
  btn: {
    backgroundColor: "#1b4332",
    marginTop: 20,
    borderRadius: 5

  },
  item: {
    borderWidth: 1,
    borderColor: "#e5e5e5",
    padding: 10,
    marginVertical: 8,
    marginHorizontal: 16,
  },
  title: {
    fontSize: 32,
  },
  subject: {
    fontSize: 20,
    fontWeight: 'bold'
  },
});
