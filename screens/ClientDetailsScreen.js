//comentario 1.60
import React, { useState, useLayoutEffect } from 'react';
import { Text, View, StyleSheet, TouchableOpacity, Image, Alert } from 'react-native';
import Constants from 'expo-constants';
import BackButton from '../components/BackButton';
import axios from 'axios';
import { REACT_APP_API_PATH, REACT_APP_URI_DASHBOARD } from '@env'
import AsyncStorage from '@react-native-async-storage/async-storage'

export default function ClientDetailsScreen({ navigation }) {

    const [user, setUser] = useState({});

    async function getClientDetails() {
        let userToken
        try {
            userToken = await AsyncStorage.getItem('userToken');
        } catch (e) {
            console.log(e);
        }

        const headers = {
            "Access-Control-Allow-Origin": "*",
            'Authorization': userToken
        }

        axios.get(REACT_APP_API_PATH + REACT_APP_URI_DASHBOARD, { headers }, { timeout: 5000 })
            .then(response => {
                console.log(response);
                setUser(response.data)
            })
            .catch(error => {
                console.log("Error status" + error.status);
            });
    }

    useLayoutEffect(() => {
        getClientDetails()
    }, []);

    return (
        <View style={styles.container}>
            <BackButton color='#000000' navigation={navigation} homePage={false} />
            <Text style={styles.txt}>Detalhes do Cliente:</Text>
            <Text style={styles.txt}>UserName : {user.clientName}</Text>
            <Text style={styles.txt}>CPF: {user.cpf}</Text>
            <Text style={styles.txt}>Phone: {user.phone}</Text>

            <TouchableOpacity
                style={[styles.btn]}
                onPress={() => getVersion()}
            >
                <Text style={{ color: '#000000', fontWeight: "bold", fontSize: 18 }}>ENTRAR</Text>
            </TouchableOpacity>
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        paddingTop: Constants.statusBarHeight,
        backgroundColor: '#fff',
        justifyContent: 'center',
        alignItems: 'center',
        padding: 8
    },
    txt: {
        fontSize: 20,
        fontWeight: 'bold',
        textAlign: 'center',
        marginBottom: 20,
        color: '#000000'
    }
});
