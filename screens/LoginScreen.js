//comentario 1.60
import { Text, View, StyleSheet, TouchableOpacity, StatusBar, Alert, Modal, Pressable, ScrollView } from 'react-native';
import Constants from 'expo-constants'
import React, { useState, useLayoutEffect } from 'react'
import axios from 'axios'
import CustomInput from '../components/CustomInput'
import { AuthContext } from '../components/context.js'
import * as Animatable from 'react-native-animatable'
import { Helmet } from "react-helmet"
import { REACT_APP_API_PATH, REACT_APP_URI_PING, REACT_APP_URI_LOGIN, VERSION } from '@env'
import * as Device from 'expo-device';
import * as Notifications from 'expo-notifications'
import AsyncStorage from '@react-native-async-storage/async-storage'

export default function LoginScreen() {
  const [version, setVersion] = useState('')
  const [userName, setUsername] = useState('')
  const [password, setPassword] = useState('')
  const [errorMessage, setErrorMessage] = useState('')
  const [modalVisible, setModalVisible] = useState(false)
  const [expoPushToken, setExpoPushToken] = useState('')
  const { signIn } = React.useContext(AuthContext)

  useLayoutEffect(() => {
    getVersion()
    registerForPushNotificationsAsync().then(
      token => setExpoPushToken(token)
    );
  }, []);

  async function clearAsyncStorage() {
    AsyncStorage.clear();
  }

  async function registerForPushNotificationsAsync() {

    let token;
    if (Device.isDevice) {
      const { status: existingStatus } = await Notifications.getPermissionsAsync();
      let finalStatus = existingStatus;
      if (existingStatus !== 'granted') {
        const { status } = await Notifications.requestPermissionsAsync()
        finalStatus = status
      }
      if (finalStatus !== 'granted') {
        alert('Failed to get push token for push notification!')
        return
      }
      token = (await Notifications.getExpoPushTokenAsync()).data
      console.log(token)
    } else {
      alert('Must use physical device for Push Notifications')
    }

    if (Platform.OS === 'android') {
      Notifications.setNotificationChannelAsync('default', {
        name: 'default',
        importance: Notifications.AndroidImportance.MAX,
        vibrationPattern: [0, 250, 250, 250],
        lightColor: '#FF231F7C',
      })
    }
    return token
  }

  async function savePushToken(expoPushToken) {
    try {
      AsyncStorage.setItem('expoPushToken', "" + expoPushToken);
    } catch (e) {
      console.log(e);
    }
  }

  const headers = {
    'Content-Type': 'application/json',
    "Access-Control-Allow-Origin": "*"
  }

  async function getVersion() {
    axios.get(REACT_APP_API_PATH + REACT_APP_URI_PING, { headers }, { timeout: 5000 })
      .then(response => {
        console.log('getting data from axios', response.data);
        setVersion("APP:" + VERSION + "   Backend: " + response.data.version)
      })
      .catch(error => {
        console.log(error);
      })
  }

  function sendLoginRequest(cpf, password) {

    savePushToken(expoPushToken)

    AsyncStorage.setItem('cpf', "" + cpf);

    const loginRequest = {
      cpf: cpf,
      password: password,
      pushToken: expoPushToken
    }

    let userToken;
    axios.post(REACT_APP_API_PATH + REACT_APP_URI_LOGIN, JSON.stringify(loginRequest), {
      headers: headers
    })
      .then(res => {
        userToken = res.data;
        console.log('user token: ', userToken);
        signIn(userToken, cpf)
      }).catch(error => {
        console.log(error);
        setErrorMessage(JSON.stringify(error))
        setModalVisible(true)
      });
  }

  const ErrorModel = (props) => {
    return (
      <Modal
        animationType="slide"
        transparent={true}
        visible={modalVisible}
        onRequestClose={() => {
          Alert.alert("Modal has been closed.");
          setModalVisible(!modalVisible);
        }}
      >
        <View style={styles.centeredView}>
          <View style={styles.modalView}>
            <ScrollView >
              <Text style={styles.modalText}> {props.error} </Text>
            </ScrollView>

            <Pressable
              style={styles.modalButton}
              onPress={() => setModalVisible(!modalVisible)}
            >
              <Text style={styles.textStyle}>Close</Text>
            </Pressable>
          </View>
        </View>
      </Modal>
    );
  }

  return (
    <Animatable.View style={styles.container}>

      <Helmet>
        <meta http-equiv='cache-control' content='no-cache' />
        <meta http-equiv='expires' content='0' />
        <meta http-equiv='pragma' content='no-cache' />
      </Helmet>

      <ErrorModel error={errorMessage} />

      <StatusBar barStyle='light-content' />

      <View style={styles.header}>
        <Animatable.Image
          animation="bounceIn"
          duraton="1500"
          resizeMode="center"
          style={{ width: '70%', height: 400, maxWidth: 100 }}
          source={require('../assets/curupira.png')} />

      </View>

      <Animatable.View
        animation="fadeInUpBig"
        style={styles.footer}>
        <Text style={styles.title}>Bem vindo!</Text>

        <View style={{ padding: 15 }}>
          <CustomInput
            placeholder="CPF"
            value={userName}
            setValue={setUsername}
          />
          <CustomInput
            placeholder="Password"
            value={password}
            setValue={setPassword}
            secureTextEntry
          />
        </View>

        <TouchableOpacity
          style={[styles.btn]}
          onPress={() => sendLoginRequest(userName, password)}
        >
          <Text style={{ color: '#fff', fontWeight: "bold", fontSize: 18 }}>ENTRAR</Text>
        </TouchableOpacity>

        <TouchableOpacity
          style={[styles.btn]}
          onPress={() => clearAsyncStorage()}>
          <Text style={{ color: '#fff', fontWeight: "bold", fontSize: 18 }}>Clear AsyncStorage</Text>
        </TouchableOpacity>

        <Text style={styles.title}>{REACT_APP_API_PATH}</Text>

      </Animatable.View>

      <Text style={[styles.version]}>{version}</Text>

    </Animatable.View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    backgroundColor: '#d8f3dc',
  },
  header: {
    paddingTop: Constants.statusBarHeight,
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center'
  },
  footer: {
    flex: 2,
    backgroundColor: '#fff',
    borderTopLeftRadius: 30
  },
  title: {
    fontSize: 32,
    fontWeight: 'bold',
    textAlign: 'center',
    marginTop: 50,
    marginBottom: 20,
    color: '#03071e'
  },
  input: {
    width: '100%',
    borderColor: '#370617',
    borderWidth: 2,
    borderRadius: 15,
    paddingHorizontal: 10,
    paddingVertical: 5,
    color: '#370617',
    marginBottom: 10
  },
  btn: {
    width: '60%',
    backgroundColor: '#1b4332',
    height: 34,
    borderRadius: 20,
    marginTop: 10,
    alignItems: 'center',
    alignSelf: 'center',
    justifyContent: 'center'
  },
  version: {
    alignSelf: 'center',
    fontWeight: 'bold'
  },
  centeredView: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    marginTop: 22
  },
  modalView: {
    margin: 20,
    backgroundColor: "white",
    borderRadius: 20,
    padding: 35,
    alignItems: "center",
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2
    },
    shadowOpacity: 0.25,
    shadowRadius: 4,
    elevation: 5
  },
  modalButton: {
    borderRadius: 20,
    padding: 10,
    elevation: 2,
    backgroundColor: "#e63946"
  },
  textStyle: {
    color: "white",
    fontWeight: "bold",
    textAlign: "center"
  },
  modalText: {
    marginBottom: 15,
    textAlign: "center",
    fontSize: 15,
    fontWeight: "bold",
    color: "#d00000"
  }
});
