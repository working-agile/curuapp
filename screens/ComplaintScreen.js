//comentario 1.60
import React, { useState } from 'react';
import { Text, View, StyleSheet, TouchableOpacity, Image, Modal, Alert, ScrollView, Pressable, ActivityIndicator } from 'react-native';
import Constants from 'expo-constants';
import BackButton from '../components/BackButton'
import CustomInput from '../components/CustomInput'
import * as ImagePicker from 'expo-image-picker'
import axios from 'axios'
import { REACT_APP_API_PATH, REACT_APP_URI_COMPLAINT } from '@env'
import * as Location from 'expo-location'
import AsyncStorage from '@react-native-async-storage/async-storage'
import * as Animatable from 'react-native-animatable'
import { format } from 'date-fns'
import { Picker } from '@react-native-picker/picker';

export default function ComplaintScreen({ navigation }) {

    const [type, setType] = useState('')

    const [address, setAddress] = useState('')
    const [cep, setCep] = useState('')

    const [image, setImage] = useState(null)

    const [location, setLocation] = useState(null);

    const [complaintProtocol, setComplaintProtocol] = useState('');

    const [modalVisible, setModalVisible] = useState(false)

    const [isLoading, setIsLoading] = useState(false)

    // const pickImage = async () => {
    //     // No permissions request is necessary for launching the image library
    //     let result = await ImagePicker.launchImageLibraryAsync({
    //         mediaTypes: ImagePicker.MediaTypeOptions.All,
    //         allowsEditing: true,
    //         quality: 1,
    //     });

    //     console.log(result);

    //     if (!result.cancelled) {
    //         setImage(result.uri);
    //     }
    // };

    const takeImage = async () => {
        let permissionResult = await ImagePicker.requestMediaLibraryPermissionsAsync();

        if (permissionResult.granted === false) {
            alert("Permission to access camera roll is required!");
            return;
        }

        let result = await ImagePicker.launchCameraAsync();
        if (!result.cancelled) {
            setImage(result.uri);
        }
    }

    const sendComplaintRequest = async () => {

        setIsLoading(true)
        let userToken
        try {
            userToken = await AsyncStorage.getItem('userToken');
        } catch (e) {
            console.log(e);
        }

        if (type == '') {
            setType("desmatamento")
        }

        const complaintRequest = {
            type: type,
            address: address,
            date: format(new Date(), 'dd/MM/yyyy H:mma')
        }

        const headers = {
            'Content-Type': 'application/json',
            "Access-Control-Allow-Origin": "*",
            'Authorization': userToken
        }
        let protocol
        axios.post(REACT_APP_API_PATH + REACT_APP_URI_COMPLAINT, JSON.stringify(complaintRequest), {
            headers: headers
        })
            .then(res => {
                protocol = res.data;
                console.log('protocolNumber: ', protocol);
                setComplaintProtocol(protocol)
                if (image) {
                    uploadImage()
                }
            }).catch(error => {
                console.log(error);
            });

    }

    const uploadImage = async () => {

        let localUri = image;
        let filename = localUri.split('/').pop();
        let match = /\.(\w+)$/.exec(filename);
        let type = match ? `image/${match[1]}` : `image`;

        let formData = new FormData();
        formData.append('photoFile', { uri: localUri, name: filename, type });

        let userToken
        try {
            userToken = await AsyncStorage.getItem('userToken');
        } catch (e) {
            console.log(e);
        }

        const headers = {
            'Content-Type': 'multipart/form-data',
            'Authorization': userToken
        }

        axios.post(REACT_APP_API_PATH + REACT_APP_URI_COMPLAINT + "/photo", formData, {
            headers: headers,
            transformRequest: (data, headers) => {
                return formData;
            },
        }).then(() => {
            setTimeout(() => {
                setIsLoading(false)
                setModalVisible(true)
            }, 200);
        }).catch(error => {
            console.log(error);
        });
    }


    const getGeoLocation = async () => {
        let { status } = await Location.requestForegroundPermissionsAsync();
        if (status !== 'granted') {
            setErrorMsg('Permission to access location was denied');
            return;
        }

        let { coords } = await Location.getCurrentPositionAsync();

        if (coords) {
            const { latitude, longitude } = coords;
            let response = await Location.reverseGeocodeAsync({
                latitude,
                longitude
            });
            console.log(JSON.stringify(response))
            for (let item of response) {
                let address = `${item.street}, ${item.name}, ${item.district}, ${item.subregion}, ${item.region}`;
                setAddress(address)
                setCep(item.postalCode)
                setLocation(address);
            }
        }
    }

    if (isLoading) {
        return (
            <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                <ActivityIndicator size="large" color={'#55a630'} />
            </View>
        );
    }

    const SuccessModel = (props) => {
        return (
            <Modal
                animationType="slide"
                transparent={true}
                visible={modalVisible}
                onRequestClose={() => {
                    Alert.alert("Modal has been closed.");
                    setModalVisible(!modalVisible);
                }}
            >
                <View style={styles.centeredView}>
                    <View style={styles.modalView}>
                        <ScrollView >
                            <Text style={styles.modalTitle}> Denúncia Realizada com sucesso!! </Text>
                            <Animatable.Image
                                animation="bounceIn"
                                duraton="1500"
                                resizeMode="center"
                                style={{ width: '70%', height: 100, maxWidth: 100, alignSelf: 'center' }}
                                source={require('../assets/happy-planet.png')} />
                            <Text style={styles.modalText}> O planeta agradece! </Text>
                            <Text style={styles.modalText}> Protocolo da Denúncia: </Text>
                            <Text style={styles.modalProtocol}> {complaintProtocol} </Text>
                        </ScrollView>

                        <Pressable
                            style={styles.modalButton}
                            onPress={() => { setModalVisible(!modalVisible), navigation.goBack() }}
                        >
                            <Text style={styles.textStyle}>Close</Text>
                        </Pressable>
                    </View>
                </View>
            </Modal>
        );
    }


    return (
        <View style={styles.container}>
            <BackButton color='#000000' navigation={navigation} homePage={false} />

            <SuccessModel />
            <ActivityIndicator />
            <Text style={styles.txt}>Realizar Denúncia:</Text>

            <Picker style={styles.picker}
                selectedValue={type}
                onValueChange={(itemValue, itemIndex) =>
                    setType(itemValue)
                }>
                <Picker.Item label="Desmatamento" value="desmatamento" />
                <Picker.Item label="Queimada" value="queimada" />
                <Picker.Item label="Poluição" value="poluicao" />
                <Picker.Item label="Caça" value="caca" />
                <Picker.Item label="Outro" value="outro" />
            </Picker>

            <CustomInput
                placeholder="Address"
                value={address}
                setValue={setAddress}
            />
            <CustomInput
                placeholder="CEP"
                value={cep}
                setValue={setCep}
            />

            <TouchableOpacity
                style={[styles.btn]}
                onPress={getGeoLocation}
            >
                <Text style={{ color: '#03071e', fontWeight: "bold", fontSize: 18 }}>UTILIZAR LOCALIZAÇÃO DO CELULAR</Text>
            </TouchableOpacity>

            <TouchableOpacity
                style={[styles.btn]}
                onPress={takeImage}
            >
                <Text style={{ color: '#03071e', fontWeight: "bold", fontSize: 18 }}>TIRAR FOTO</Text>
            </TouchableOpacity>

            {image && <Image source={{ uri: image }} style={{ width: 100, height: 100 }} />}

            <Text style={{ color: '#03071e', fontWeight: "bold", fontSize: 18, padding: 10 }}>{JSON.stringify(location)}</Text>

            <TouchableOpacity
                style={[styles.btn]}
                onPress={sendComplaintRequest} >
                <Text style={{ color: '#03071e', fontWeight: "bold", fontSize: 18 }}>ENVIAR DENÚNCIA</Text>
            </TouchableOpacity>


        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        paddingTop: Constants.statusBarHeight,
        backgroundColor: '#fff',
        justifyContent: 'center',
        alignItems: 'center',
        padding: 8
    },
    txt: {
        fontSize: 20,
        fontWeight: 'bold',
        textAlign: 'center',
        marginBottom: 20,
        color: '#000000'
    },
    input: {
        width: '100%',
        borderColor: '#370617',
        borderWidth: 2,
        borderRadius: 15,
        paddingHorizontal: 10,
        paddingVertical: 5,
        color: '#370617',
        marginBottom: 20
    },
    picker: {
        width: '100%',
        borderColor: '#370617',
        borderWidth: 2,
        borderRadius: 15,
        paddingHorizontal: 10,
        paddingVertical: 5,
        color: '#370617',
        marginTop: 20
    },

    btn: {
        backgroundColor: '#d9ed92',
        borderRadius: 10,
        paddingLeft: 15,
        paddingRight: 15,
        paddingBottom: 5,
        marginTop: 15
    },
    modalView: {
        margin: 20,
        backgroundColor: "white",
        borderRadius: 20,
        padding: 35,
        alignItems: "center",
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2
        },
        shadowOpacity: 0.25,
        shadowRadius: 4,
        elevation: 5
    },
    modalButton: {
        borderRadius: 20,
        padding: 10,
        elevation: 2,
        backgroundColor: "#3a5a40"
    },
    textStyle: {
        color: "white",
        fontWeight: "bold",
        textAlign: "center"
    },
    modalText: {
        marginBottom: 15,
        textAlign: "center",
        fontSize: 15,
        fontWeight: "bold",
        color: "#90be6d"
    },
    modalTitle: {
        marginBottom: 15,
        textAlign: "center",
        fontSize: 25,
        fontWeight: "bold",
        color: "#90be6d"
    },
    modalProtocol: {
        marginBottom: 15,
        textAlign: "center",
        fontSize: 25,
        fontWeight: "bold",
        color: "#90be6d"
    },
});
