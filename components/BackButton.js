import * as React from 'react';
import { StyleSheet, TouchableOpacity } from 'react-native';
import { Ionicons } from '@expo/vector-icons';
import Constants from 'expo-constants';
import { AuthContext } from '../components/context.js'
export default function App(props) {
  const { signOut } = React.useContext(AuthContext);
  return (
    <TouchableOpacity
      style={styles.container}
      onPress={() => {
        if (props.homePage == false) {
          props.navigation.goBack()
        } else {
          signOut()
        }
      }}
    >
      <Ionicons name='arrow-back' size={24} color={props.color} />
    </TouchableOpacity>
  );
}

const styles = StyleSheet.create({
  container: {
    position: 'absolute',
    top: (Constants.statusBarHeight + 10),
    left: 10
  }
});