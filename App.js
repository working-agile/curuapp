import React, { useEffect, useState, useRef } from 'react'
import { NavigationContainer } from '@react-navigation/native'
import { View, ActivityIndicator, Alert, Modal, Pressable, Text } from 'react-native'
import HomeScreen from './screens/HomeScreen'
import LoginScreen from './screens/LoginScreen'
import AsyncStorage from '@react-native-async-storage/async-storage'
import { createNativeStackNavigator } from '@react-navigation/native-stack'
import { AuthContext } from './components/context.js'
import ClientDetailsScreen from './screens/ClientDetailsScreen'
import * as Notifications from 'expo-notifications'
import { format } from 'date-fns'

import ComplaintScreen from './screens/ComplaintScreen'
const MainStack = createNativeStackNavigator()

Notifications.setNotificationHandler({
  handleNotification: async () => ({
    shouldShowAlert: true,
    shouldPlaySound: false,
    shouldSetBadge: false,
  }),
})

export default function App() {

  const _handleNotification = async (notification) => {
    saveNewMessage(notification.request.trigger.remoteMessage.data.title, notification.request.trigger.remoteMessage.data.message)
  }

  async function saveNewMessage(subject, content) {
    try {

      let newMessage = {
        "subject": subject,
        "content": content,
        "date": format(new Date(), 'dd/MM/yyyy H:mma')
      }

      let numberOfMessages = await AsyncStorage.getItem('numberOfMessages')

      if (isNaN(numberOfMessages)) {
        numberOfMessages = 1
      } else {
        numberOfMessages++
      }

      AsyncStorage.setItem('message-' + numberOfMessages, "" + JSON.stringify(newMessage))
      AsyncStorage.setItem('numberOfMessages', "" + numberOfMessages)

    } catch (error) {
      console.log(error)
    }
  }

  const _handleNotificationResponse = response => {
    console.log(response);
  }

  const [erro, setErro] = useState('')

  const initialLoginState = {
    isLoading: true,
    cpf: null,
    userToken: null
  }

  const loginReducer = (prevState, action) => {
    switch (action.type) {
      case 'RETRIEVE_TOKEN':
        return {
          ...prevState,
          userToken: action.token,
          isLoading: false,
        }
      case 'LOGIN':
        return {
          ...prevState,
          cpf: action.id,
          userToken: action.token,
          isLoading: false,
        }
      case 'LOGOUT':
        return {
          ...prevState,
          cpf: null,
          userToken: null,
          isLoading: false,
        }
    }
  }

  const [loginState, dispatch] = React.useReducer(loginReducer, initialLoginState);

  const authContext = React.useMemo(() => ({
    signIn: async (userToken, cpf) => {
      try {
        AsyncStorage.setItem('userToken', "" + userToken);
      } catch (e) {
        console.log(e);
      }
      dispatch({ type: 'LOGIN', id: cpf, token: userToken });
    },

    signOut: async () => {
      try {
        await AsyncStorage.removeItem('userToken');
      } catch (e) {
        console.log(e);
      }
      dispatch({ type: 'LOGOUT' });
    }
  }), []);


  useEffect(() => {

    Notifications.addNotificationReceivedListener(_handleNotification);
    Notifications.addNotificationResponseReceivedListener(_handleNotificationResponse);

    setTimeout(async () => {

      let userToken;
      try {
        userToken = await AsyncStorage.getItem('userToken');
      } catch (e) {
        console.log(e);
      }
      console.log('user token: ', userToken);
      dispatch({ type: 'RETRIEVE_TOKEN', token: userToken });
    }, 1000)
  }, [])


  if (loginState.isLoading) {
    return (
      <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
        <ActivityIndicator size="large" />
      </View>
    );
  }

  return (
    <AuthContext.Provider value={authContext}>
      <NavigationContainer>
        {loginState.userToken != null ? (
          <MainStack.Navigator screenOptions={{
            headerShown: false
          }}>
            <MainStack.Screen name='HomeScreen' component={HomeScreen} />
            <MainStack.Screen name='ClientDetailsScreen' component={ClientDetailsScreen} />
            <MainStack.Screen name='ComplaintScreen' component={ComplaintScreen} />
          </MainStack.Navigator>
        ) :
          <MainStack.Navigator screenOptions={{
            headerShown: false
          }}>
            <MainStack.Screen name='LoginScreen' component={LoginScreen} />
          </MainStack.Navigator>
        }
      </NavigationContainer>
    </AuthContext.Provider>
  );
}

